import styles from "./NextPage.module.scss";
import { FaChevronLeft, FaChevronRight } from "react-icons/fa";

function NextPage(props) {

    return (
        <>
            <main>
                <section className={styles.next_page}>
                    <div className={styles.container}>
                        <div className={styles.next}>
                            <div className="row">
                                <div className="coluna">
                                    <ul>
                                        <li><FaChevronLeft /></li>
                                        <li>01</li>
                                        <li>02</li>
                                        <li>03</li>
                                        <li>04</li>
                                        <li><FaChevronRight /></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </>
    )
}

export default NextPage;