import styles from "./InfoLeft.module.scss";

function InfoLeft(props) {

    return (
        <>
            <section className={styles.servico_left}>
                <div className={styles.container}>
                    <div className={styles.background}>
                        <div className={styles.row}>
                            <div className={styles.flex2}>
                                <div className={styles.img2} style={{ 'backgroundImage': `url('/assets/img/variedade.jpg')` }}></div>
                            </div>
                            <div className={styles.col_texto}>
                                <h1 className={styles.texto_dir}>Variedades</h1>
                                <p className={styles.p_dir}>
                                    Trazendo tudo o que há de mais novo no mercado, 
                                    nós oferecemos peças inovadoras, e o melhor: limitadas. 
                                    Temos a consciência de que a inovação e criatividade 
                                    é o que nos motiva, e nos destaca. 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default InfoLeft;