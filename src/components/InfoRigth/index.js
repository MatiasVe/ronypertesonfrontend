import styles from "./InfoRigth.module.scss";

function InfoRigth(props) {

    return (
        <>
            <section className={styles.servico_rigth}>
                <div className={styles.container}>
                    <div className={styles.background}>
                        <div className={styles.row}>
                            <div className={styles.col_texto}>
                                <h1 className={styles.texto_left}>Qualidade e capricho</h1>
                                <p className={styles.p_left}>
                                    Baseado em todo o mercado,Rony Peterson revolveu mais uma vez 
                                    na sua carreira, reinventar. Analisando todo o procedimento existente na área, 
                                    e, tomando a consciência de que a famosa “mesmice” era o que 
                                    predominava, teve a ideia de trazer algo novo.
                                </p>
                            </div>
                            <div className={styles.flex1}>
                                <div className={styles.img1} style={{ 'backgroundImage': `url('/assets/img/qualidade.jpg')` }}></div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default InfoRigth;