import styles from "./Produto.module.scss";
import { FaTruck, FaTruckMoving, FaPlayCircle } from 'react-icons/fa';
import { IoMdAnalytics } from "react-icons/io";
import { BsArrowRight } from "react-icons/bs";

function InformacoesFlutuante(props) {


    return (
        <>
            <section className={styles.informacoes_fluante}>
                <div className="container">
                    <div className="row">
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/laco0.jpg')` }}></div>
                        </div>
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/laco1.jpg')` }}></div>
                        </div>
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/laco2.jpg')` }}></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/laco3.jpg')` }}></div>
                        </div>
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/laco4.jpg')` }}></div>
                        </div>
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/variedade.jpg')` }}></div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/variedade.jpg')` }}></div>
                        </div>
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/variedade.jpg')` }}></div>
                        </div>
                        <div className="coluna">
                            <div className={styles.flutuante} style={{ 'backgroundImage': `url('/assets/img/variedade.jpg')` }}></div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    )
}

export default InformacoesFlutuante;