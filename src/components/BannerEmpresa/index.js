import styles from "./BannerEmpresa.module.scss";
import Slider from "react-slick";

function BannerEmpresa(props) {
    
    return (
        <>
            <section className={styles.banner_empresa}>
                <div>
                    <div className={styles.slider} style={{ 'backgroundImage': `url('/assets/img/banner-teste.jpg')` }}></div>
                </div>
            </section>
        </>
    )
}

export default BannerEmpresa;