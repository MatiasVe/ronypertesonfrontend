import styles from "./Aguarda.module.scss";
import { BsArrowRight } from "react-icons/bs";


function Aguarda(props) {

    return (
        <>
            <section className={styles.sobre_cgi} style={{ 'backgroundImage': `url('/assets/img/img.jpg')` }}>
                <div className="container">
                    <div className="row">
                        <div className="coluna">
                        <div className={styles.contents}>
                            <h1>SEU PET JÁ AGUARDA ANCIOSO</h1>
                            <a href="#">
                                PEÇA JÁ O SEU
                                <BsArrowRight className={styles.arrow} />
                            </a>
                        </div>
                        </div>
                        {/* <div className={styles.coluna}>
                            <div className={styles.truck} style={{ 'backgroundImage': `url('/assets/img/img-empresa1.png')` }}></div>
                            <img src="/assets/img/img-empresa1.png" alt="Soluções"/>
                        </div> */}
                    </div>
                </div>
            </section>
        </>
    )
}

export default Aguarda;