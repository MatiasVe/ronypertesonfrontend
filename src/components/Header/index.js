import styles from './Header.module.scss';
import Image from 'next/image';
import {useState, useEffect } from 'react';
import Link from 'next/link';
import { FaUser} from 'react-icons/fa';

function Header() {
  const [active_header, setActive_header] = useState(false)
  const [active_menu_mobile, setActive_menu_mobile] = useState(false)
  useEffect(function onFirstMount() {
    function onScroll() {
      if(window.scrollY > 0){
        setActive_header(true)
        console.log('true')
      }
      else{
        setActive_header(false)
        console.log('false')
      }
    }
    window.addEventListener("scroll", onScroll);
    return () => {
      window.removeEventListener("scroll", onScroll); //cleanUp
    }
  }, []);
  return (
    <>
    <header className={`${styles.header} ${active_header ? styles.active_header : ''}`}>
      <div className="container">
        <div className="row">
          <div className="coluna">
            <a href="/">
              <img src="/assets/img/logo.png" alt="logo" width={100} height={100}/>
            </a>
          </div>
          <div className={styles.coluna}>
            <nav>
              <ul>
                <li>
                <Link href="/">
                  <a>HOME</a>
                </Link>
                </li>
                <li>
                <Link href="/empresa">
                  <a>A EMPRESA</a>
                </Link>
                </li>
                <li>
                <Link href="/produtos">
                  <a>PRODUTOS</a>
                </Link>
                </li>
                <li>
                  <Link href="/contato">
                    <a>CONTATO</a>
                  </Link>
                </li>
              </ul>
            </nav>
            <div className={styles.toggle} onClick={()=>{setActive_menu_mobile(!active_menu_mobile)}}>
              <span></span>
              <span></span>
              <span></span>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div className={`${styles.menu_mobile} ${active_menu_mobile ? styles.active_menu_mobile : ''}`}>
      <ul>
        <li>
          <a href="/">HOME</a>
        </li>
        <li>
          <a href="/empresa">A EMPRESA</a>
        </li>
        <li>
          <a href="/servicos">SERVIÇOS</a>
        </li>
        <li>
          <a href="/blog">BLOG</a>
        </li>
        <li>
          <a href="/contato">CONTATO</a>
        </li>
        <li>
          <a href="/login">ÁREA DO CLIENTE</a>
        </li>
      </ul>
    </div>
    </>
  );
}

export default Header;