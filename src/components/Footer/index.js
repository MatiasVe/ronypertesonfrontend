import styles from './Footer.module.scss';
import Image from 'next/image'
import {useState, useEffect } from 'react';
import { FaInstagram,FaWhatsapp, FaFacebookF} from 'react-icons/fa';

function Footer(){
  return(
    <footer className={styles.footer}>
      <div className="container">
        <div className="row">
          <div className={styles.logo}>
            <img src="/assets/img/logo.png" alt=""/>
          </div>
          <div className={styles.coluna}>
            <h1>A Luxury For Pets</h1>
            <p className={styles.p_um}>
              É a marca que veio para revolucionar o mercado de embelezamento pet. 
              Trazendo tudo o que há de mais novo no mercado, nós oferecemos peças 
              inovadoras, e o melhor: limitadas. Temos a consciência de que a inovação 
              e criatividade é o que nos motiva, e nos destaca. 
            </p>
          </div>
          {/* <div className={styles.coluna}>
          <h1>Onde estamos</h1>
          <p>
            Av. Dom Luís, 812 - Aldeota, 
            Fortaleza - CE, 60160-196
          </p>
          </div> */}
          <div className={styles.coluna}>
          <h1>Missão</h1>
          <p>
            Temos como missão a inovação. 
            Somos visionários, e pensamos sempre a frente. 
            Essa habilidade nos possibilita inovar, e como 
            já dito, nos destacar no meio. Temos como intuito, 
            mostrar que viemos ser a diferença. 
          </p>
          </div>
          <div className={styles.coluna}>
            <h1>Contato</h1>
           <p>
            Av. Dom Luís, 812-Aldeota, 
            Fortaleza-CE.
           </p>
           <p>Tel:(85) 98888-8888</p>
            <ul>
              <li><a href="https://www.instagram.com/petersonforpets/" target="_blank" className={styles.instagram}><FaInstagram/></a></li>
              <li><a href="https://api.whatsapp.com/message/KKFC5REFXEFWH1" target="_blank" className={styles.whatsapp}><FaWhatsapp/></a></li>
            </ul>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer;