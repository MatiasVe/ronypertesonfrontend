import Document, {Html, Head, Main, NextScript} from 'next/document';

class NewDocument extends Document {
  static async  getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx)
    return {...initialProps}
  }

  render(){
    return(
      <Html lang="pt-br">
        <Head>
          <>
            <title>RonyPeterson</title>
            {/* <link rel="shortcut icon" href="https://futeboles.com.br/favicon.ico" /> */}
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="author" content="WebExclusiva"></meta>
            <meta name="robots" content="all" />
            <meta name="description" content="GCIRisk, site em desenvolvimento"/>
            <meta name="keywords" content="rastreamento veicular, rastreamento de cargas"></meta>
            <meta property="og:title" content="RonyPeterson"></meta>
            <meta property="og:type" content="website"></meta>
            <meta property="og:url" content="https://www.gcirisk.com.br"></meta>
            {/* <meta property="og:image" content="../../public/assets/img/palpitaco-seo.jpg"></meta>
            <meta property="og:image:secure_url" content="../../public/assets/img/palpitaco-seo.jpg"></meta> */}
            {/* <meta property="og:image:type" content="image/jpeg"></meta>
            <meta property="og:image:width" content="1024"></meta>
            <meta property="og:image:height" content="1024"></meta> */}
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css"></link>
          </> 
        </Head>
        <body>
          <Main />
          <NextScript />
          
        </body>
      </Html>
    )
  }
}

export default NewDocument;