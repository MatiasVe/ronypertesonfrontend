import Header from "../components/Header";
import BannerProduto from "../components/BannerProduto";
import Titulo from "../components/Titulo";
import Footer from "../components/Footer";
import Produtos from "../components/Produtos";
import NextPage from "../components/NextPage"

function Index(props) {
  return (
    <>
      <Header/>
      <BannerProduto/>
      <Produtos/>
      <NextPage/>
      <Footer/>
    </>
  )
}

export default Index;