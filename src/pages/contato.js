import Header from "../components/Header";
import Contato from "../components/Contato";
import Footer from "../components/Footer";
import Aguarda from "../components/Aguarda";

function Index(props) {
  return (
    <>
      <Header/>
      <Contato/>
      <Aguarda/>
      <Footer/>
    </>
  )
}

export default Index;